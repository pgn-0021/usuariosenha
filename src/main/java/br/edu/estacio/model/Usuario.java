package br.edu.estacio.model;

import com.db4o.config.annotations.Indexed;

public class Usuario {

	@Indexed
	private String nome;

	private Senha senha =  new Senha();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Senha getSenha() {
		return senha;
	}
		
}
