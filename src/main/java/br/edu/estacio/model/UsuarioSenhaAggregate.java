package br.edu.estacio.model;

public class UsuarioSenhaAggregate {

	private Usuario usr = new Usuario();
	
	public UsuarioSenhaAggregate(String nome, String senha) {
		usr.setNome(nome);
		usr.getSenha().setSenha(senha);
	}
	
	public String getSenha() {
		return usr.getSenha().getSenha();
	}
	
}
