package br.edu.estacio.Principal;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.Db4oException;

import br.edu.estacio.model.UsuarioSenhaAggregate;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UsuarioSenhaAggregate us1 = new UsuarioSenhaAggregate("Admin","admin");
		UsuarioSenhaAggregate us2 = new UsuarioSenhaAggregate("dba","12345");
		UsuarioSenhaAggregate us3 = new UsuarioSenhaAggregate("ad","123456");
		
		
		ObjectContainer db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), "/home/dell/data.dbo");
		
		try {
			db.store(us1);
			db.store(us2);
			db.store(us3);
			db.commit();
		}catch (Db4oException e) {
			db.rollback();
		}
		
		db.close();

		db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), "/home/dell/data.dbo");
		
		ObjectSet<UsuarioSenhaAggregate> obs = db.queryByExample(new UsuarioSenhaAggregate("ad","42321"));
		if (obs.hasNext()) {
			UsuarioSenhaAggregate usa = obs.next();
			System.out.println("A senha do usuário é :"+ usa.getSenha());
		} else {
			System.out.println("Não foi possível achar o usuário!!");
		}
		db.close();
		
	}

}
